(function ($) {
    Drupal.behaviors.gearVerticalHeader = {
        attach: function (context) {
            $(".vertical", context).each(function () {
                var width = $(this).outerWidth();

                $(this).css("height", width + "px");
                $(this).css("width", "1px");
                $(this).css("maxWidth", "1px");
            });
        }
    };
}(jQuery));
