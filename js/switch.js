(function ($) {
    Drupal.behaviors.bootstrapSwitch = {
        attach: function (context) {
            $('.bs_switch', context).each(function () {
                $(this).parent().css('margin-bottom', '0');
                $(this).parent().parent().removeClass('checkbox');
                $(this).bootstrapSwitch();
            });
        }
    };
})(jQuery);
