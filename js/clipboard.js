(function ($) {
    Drupal.behaviors.clipboard = {
        attach: function (context) {
            new Clipboard('.btn-copy', context);
        }
    };
})(jQuery);
