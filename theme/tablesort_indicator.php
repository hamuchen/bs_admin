<?php

/**
 * theme_tablesort_indicator
 *
 * @param array $vars
 *
 * @return string
 */
function bs_admin_tablesort_indicator($vars) {
  if ($vars['style'] == "asc") {
    $desc = t('sort ascending');

    return '<span class="glyphicon glyphicon-triangle-bottom small"
    alt="' . $desc . '" title="' . $desc . '"></span>';
  }
  else {
    $desc = t('sort descending');

    return '<span class="glyphicon glyphicon-triangle-top small"
    alt="' . $desc . '" title="' . $desc . '"></span>';
  }
}
