<?php

/**
 * theme_ctools_wizard_trail
 *
 * @param array $vars
 *
 * @return string
 */
function bs_admin_ctools_wizard_trail($vars) {
  if (!empty($vars['trail'])) {
    $icon = '<span class="glyphicon glyphicon-menu-right" ></span>';

    return '<div class="wizard-trail">' .
      implode($icon, $vars['trail']) . '</div>';
  }

  return '';
}
