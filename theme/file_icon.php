<?php

/**
 * theme_file_icon()
 */
function bs_admin_file_icon($variables) {
  $file = $variables['file'];
  $class = bs_admin_icons_class($file);
  $mime = check_plain($file->filemime);

  return '<i class="file-icon fa fa-' . $class . ' fa-lg" alt="" title="' . $mime . '" ></i>';
}

function bs_admin_icons_class($file) {
  $font_awesome = array(
    'application-octet-stream' => 'file-o',
    'application-pdf'          => 'file-pdf-o',
    'application-x-executable' => 'file-o',
    'audio-x-generic'          => 'file-audio-o',
    'image-x-generic'          => 'file-image-o',
    'package-x-generic'        => 'file-archive-o',
    'text-html'                => 'file-code-o',
    'text-plain'               => 'file-text-o',
    'text-x-generic'           => 'file-text-o',
    'text-x-script'            => 'file-code-o',
    'video-x-generic'          => 'file-video-o',
    'x-office-document'        => 'file-word-o',
    'x-office-presentation'    => 'file-powerpoint-o',
    'x-office-spreadsheet'     => 'file-excel-o',
  );

  // If there's an icon matching the exact mimetype, go for it.
  $dashed_mime = strtr($file->filemime, array('/' => '-'));
  if (array_key_exists($dashed_mime, $font_awesome)) {
    return $font_awesome[$dashed_mime];
  }

  // For a few mimetypes, we can "manually" map to a generic icon.
  $generic_mime = (string) file_icon_map($file);
  if (array_key_exists($generic_mime, $font_awesome)) {
    return $font_awesome[$generic_mime];
  }

  // Use generic icons for each category that provides such icons.
  foreach (array('audio', 'image', 'text', 'video') as $category) {
    if (strpos($file->filemime, $category . '/') === 0) {
      $key = $category . '-x-generic';
      if (array_key_exists($key, $font_awesome)) {
        return $font_awesome[$key];
      }
    }
  }
}
