<?php

/**
 * theme_file_link()
 *
 * @param array $vars
 *
 * @return string
 */
function bs_admin_file_link($vars) {
  $file = $vars['file'];
  $icon_directory = $vars['icon_directory'];

  $url = file_create_url($file->uri);
  $icon = theme('file_icon',
                array('file' => $file, 'icon_directory' => $icon_directory));

  // Set options as per anchor format described at
  // http://microformats.org/wiki/file-format-examples
  $options = array(
    'attributes' => array(
      'type' => $file->filemime . '; length=' . $file->filesize,
    ),
    'html'       => TRUE,
  );

  // Use the description as the link text if available.
  if (empty($file->description)) {
    $link_text = $file->filename;
  }
  else {
    $link_text = $file->description;
    $options['attributes']['title'] = check_plain($file->filename);
  }

  // put icon into link text
  $link_text = $icon . ' ' . $link_text;

  return '<span class="file">' . l($link_text, $url, $options) . '</span>';
}
