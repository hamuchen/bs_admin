<?php

/**
 * theme_advanced_link
 *
 * @param $vars
 *
 * @return string
 */
function bs_admin_advanced_link($vars) {
  return bs_admin_link_field($vars);
}
