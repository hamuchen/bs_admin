<?php

/**
 * theme_link_field
 *
 * @param $vars
 *
 * @return string
 */
function bs_admin_link_field($vars) {
  drupal_add_css(drupal_get_path('module', 'link') . '/link.css');
  $element = $vars['element'];

  // Prefix single value link fields with the name of the field.
  if (empty($element['#field']['multiple'])) {
    if (isset($element['url']) && !isset($element['title'])) {
      $element['url']['#title_display'] = 'invisible';
    }
  }

  $element['url']['#attributes']['placeholder'] = $element['url']['#title'];

  $output = '';
  $output .= '<div class="link-field-subrow row">';
  if (isset($element['title'])) {
    $element['title']['#attributes']['placeholder'] = $element['title']['#title'];
    $output .= '<div class="link-field-title col-sm-6">' . drupal_render($element['title']) . '</div>';
  }
  $col_class = isset($element['title']) ? '6' : '12';
  $output .= '<div class="link-field-url col-sm-' . $col_class . '">' . drupal_render($element['url']) . '</div>';
  $output .= '</div>';
  if (!empty($element['attributes']['target'])) {
    $output .= '<div class="link-attributes">' . drupal_render($element['attributes']['target']) . '</div>';
  }
  if (!empty($element['attributes']['title'])) {
    $output .= '<div class="link-attributes">' . drupal_render($element['attributes']['title']) . '</div>';
  }
  if (!empty($element['attributes']['class'])) {
    $output .= '<div class="link-attributes">' . drupal_render($element['attributes']['class']) . '</div>';
  }
  $output .= drupal_render_children($element);

  return $output;
}
