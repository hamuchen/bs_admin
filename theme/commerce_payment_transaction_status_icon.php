<?php
/**
 * theme_commerce_payment_transaction_status_icon
 *
 * @param array $vars
 *
 * @return string
 */
function bs_admin_commerce_payment_transaction_status_icon($vars) {
  $status = $vars['transaction_status']['status'];

  $icon_class = '';
  if ('success' == $status) {
    $icon_class = 'check-circle text-success';
  }
  elseif ('failure' == $status) {
    $icon_class = 'exclamation-circle text-danger';
  }
  elseif ('pending' == $status) {
    $icon_class = 'clock-o text-warning';
  }

  return '<i class="fa fa-' . $icon_class . '"></i>';
}
