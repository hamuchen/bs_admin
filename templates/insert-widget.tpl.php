<?php

/**
 * @file
 * Template file for the insert button.
 *
 * This button can have any appearance you like or even be a link, but must
 * include the class "insert-button", to which the insert JavaScript will be
 * attached.
 */
?>
<?php if (count($insert_styles) > 1): ?>
  <select class="form-control insert-style">
    <?php foreach ($insert_styles as $value => $style): ?>
      <option
          value="<?php print $value ?>"<?php print ($value == $default_style) ?
        'selected="selected"' : '' ?>><?php print $style ?></option>
    <?php endforeach; ?>
  </select>
<span class="input-group-btn">
  <?php else: ?>
    <input type="hidden" class="insert-style"
           value="<?php print $default_style ?>" />
  <?php endif; ?>

  <button type="submit" rel="<?php print $widget_type ?>"
          class="btn btn-default insert-button"
          onclick="return false;"><?php print t('Insert'); ?></button>

  <?php if (count($insert_styles) > 1): ?>
  </span>
<?php endif; ?>
