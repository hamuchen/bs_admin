<?php

################################################################################
## REQUIRE THEME DIRECTORY

foreach (glob(dirname(__FILE__) . '/theme/*.php') as $file) {
  include_once $file;
}

################################################################################
## JAVASCRIPT STUFF

/**
 * hook_js_alter
 *
 * @param array $js
 */
function bs_admin_js_alter(&$js) {
  bs_admin_md_slider($js);
}

/**
 * remove specific js (mostly multiple jquery) for md_slider to work properly
 *
 * @param array $js
 */
function bs_admin_md_slider(&$js) {
  if (arg(0) != 'admin' || arg(1) != 'structure' || arg(2) || 'md-slider') {
    return;
  }

  $paths = array(
    drupal_get_path('theme', 'bs_admin'),
    drupal_get_path('theme', 'bootstrap'),
    drupal_get_path('module', 'jquery_update'),
    drupal_get_path('module', 'adminimal_admin_menu'),
  );
  foreach ($js as $key => $value) {
    foreach ($paths as $path) {
      if (strpos($key, $path) === 0) {
        unset($js[$key]);
      }
    }
  }
}

################################################################################
## PREPROCESS HOOKS

/**
 * hook_process_page
 *
 * @param array $vars
 */
function bs_admin_process_page(&$vars) {
  // Set local tasks variables
  $vars['tabs_primary'] = menu_primary_local_tasks();
  if (!empty($vars['tabs_primary'])) {
    $vars['tabs_primary']['#prefix'] =
      '<h2 class="element-invisible">' . t('Primary tabs') . '</h2>';
    $vars['tabs_primary']['#prefix'] .= '<ul class="tabs--primary nav nav-tabs navbar-right">';
    $vars['tabs_primary']['#suffix'] = '</ul>';
  }

  $vars['tabs_secondary'] = menu_secondary_local_tasks();
  if (!empty($vars['tabs_secondary'])) {
    $vars['tabs_secondary']['#prefix'] =
      '<h2 class="element-invisible">' . t('Secondary tabs') . '</h2>';
    $vars['tabs_secondary']['#prefix'] .= '<ul class="tabs--secondary pagination pull-right">';
    $vars['tabs_secondary']['#suffix'] = '</ul>';
  }
}

/**
 * hook_preprocess_table
 *
 * @param array $vars
 */
function bs_admin_preprocess_table(&$vars) {
  //  dpm($vars);
  $vars['attributes']['class'][] = 'table';
  $vars['attributes']['class'][] = 'table-striped';

  if (!isset($vars['attributes']['id'])) {
    return;
  }

  // user permission table overrides
  if ($vars['attributes']['id'] == 'permissions') {
    foreach ($vars['header'] as $key => $header) {
      if ($key == 0) {
        continue;
      }
      $vars['header'][$key]['class'][] = 'vertical';
      $vars['header'][$key]['data'] =
        '<div>' . $vars['header'][$key]['data'] . '</div>';
    }
  }

  // user role table overrides
  if ($vars['attributes']['id'] == 'user-roles') {
    $vars['header'][2]['class'][] = 'fix-width-1';
  }

  // menu overview table overrides
  if ($vars['attributes']['id'] == 'menu-overview') {
    $vars['header'][1]['class'][] = 'vertical';
    $vars['header'][1]['data'] =
      '<div>' . $vars['header'][1]['data'] . '</div>';
  }

  // book overview table overrides
  if ($vars['attributes']['id'] == 'book-outline') {
    $vars['header'][3]['class'][] = 'fix-width-1';
  }
}

/**
 * better_exposed_filters override
 *
 * @param array $vars
 */
function bs_admin_preprocess_select_as_checkboxes(&$vars) {
  $element = &$vars['element'];
  // Remove form-control class added to original "select" element
  if (($key =
      array_search('form-control', $element['#attributes']['class'])) !== FALSE
  ) {
    unset($element['#attributes']['class'][$key]);
  }
}

################################################################################
## RENDER HOOKS

/**
 * Implements hook_pre_render().
 *
 * @param array $element
 *
 * @return array
 */
function bs_admin_pre_render($element) {
  if (!empty($element['#bootstrap_ignore_pre_render'])) {
    return $element;
  }

  // Add necessary classes for specific types.
  if ($type = !empty($element['#type']) ? $element['#type'] : FALSE) {
    // fontawesome_iconpicker
    if ($type == 'fontawesome_iconpicker_textfield') {
      $element['#attributes']['class'][] = 'form-control';
    }
  }

  // views admin rewrite results
  if ($id = !empty($element['#id']) ? $element['#id'] : FALSE) {
    if ($id == 'edit-options-alter-text') {
      if (($key = array_search(
          'form-control', $element['#attributes']['class'])) !== FALSE
      ) {
        unset($element['#attributes']['class'][$key]);
      }
    }
  }

  return $element;
}

################################################################################
## FORM HOOKS

/**
 * hook_form_alter
 *
 * @param array  $form
 * @param array  $form_state
 * @param string $form_id
 */
function bs_admin_form_alter(&$form, &$form_state, $form_id) {
  // Reorder vertical tabs
  if (isset($form['revision_information'])) {
    $form['revision_information']['#weight'] = -10;
  }
  if (isset($form['options'])) {
    $form['options']['#weight'] = -9;
  }
  if (isset($form['author'])) {
    $form['author']['#weight'] = -8;
  }
}

/**
 * hook_field_widget_WIDGET_TYPE_form_alter
 *
 * @param array $element
 * @param array $form_state
 * @param array $context
 */
function bs_admin_field_widget_options_onoff_form_alter(&$element, &$form_state,
                                                        $context) {
  // alter element to be used with bootstrap switch
  $instance = field_info_instance($element['#entity_type'],
                                  $element['#field_name'],
                                  $element['#bundle']);
  if (isset($instance['widget']['settings']['display_label']) && $instance['widget']['settings']['display_label']) {
    $element['#attributes']['class'][] = 'bs_switch';
    // label
    $element['#attributes']['data-label-text'] = $element['#title'];
    $element['#title_display'] = 'invisible';
    // values
    $field = field_info_field($element['#field_name']);
    $allowed_values = list_allowed_values($field);
    $element['#attributes']['data-on-text'] = $allowed_values[$element['#on_value']];
    $element['#attributes']['data-off-text'] = $allowed_values[$element['#off_value']];
  }
}

################################################################################
## BOOTSTRAP BUTTONS

/**
 * hook_bootstrap_colorize_text_alter
 *
 * @param array $texts
 */
function bs_admin_bootstrap_colorize_text_alter(&$texts) {
  $texts['matches'][t('Continue')] = 'success';
  $texts['matches'][t('Finish')] = 'success';
}

/**
 * hook_bootstrap_iconize_text_alter
 *
 * @param array $texts
 */
function bs_admin_bootstrap_iconize_text_alter(&$texts) {
  $texts['matches'][t('Import')] = 'import';
}
